﻿public class PhotoProcessor
{
    #region Delegate
    public delegate void Delegado(Photo photo);

    //public void ProcessD(String path, Action<Photo> filterHandler)
    public void ProcessD(string path, Delegado filterHandler)
    {
        var photo = new Photo();

        //Esta clase no sabe qué filtro se le va a aplicar
        filterHandler(photo);
        //Se vuelve responsabilidad del cliente de este código definir los filtros que quiere.

        photo.Save();
    }

    #endregion
    public void Process(string path)
    {
        var photo = new Photo();
        
        var filters = new PhotoFilters();
        filters.ApplyBrightness(photo);
        filters.ApplyContrast(photo);
        filters.Resize(photo);
        //Qué pasa si quiero agregar otro filtro al procesado de la foto?
        //Tendría que modificar la clase de filtros y esta también. Re comiplar, desplegar, etc... No es mantenible.

        photo.Save();
    }
}
