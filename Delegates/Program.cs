﻿class Program
{
    static void Main(string[] args)
    {
        var processor = new PhotoProcessor();

        #region Delegate

        var filters = new PhotoFilters();
        PhotoProcessor.Delegado filterHandler = filters.ApplyBrightness;
        //Action<Photo> filterHandler = filters.ApplyBrightness;

        filterHandler += filters.Resize;
        filterHandler += filters.ApplyContrast;
        filterHandler += AnotherOne;

        processor.ProcessD("photo.jpg", filterHandler);

        #endregion

        //processor.Process("photo.jpg");
        
    }

    #region External coded filter
    static void AnotherOne(Photo photo)
    {
        Console.WriteLine("Filtro creado aparte del resto de clases");
    }
    #endregion
}