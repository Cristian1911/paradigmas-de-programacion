﻿using System.Threading.Channels;


var video = new Video() { Title = "Video 1"};
var videoEncoder = new VideoEncoder(); //publisher

var mailService = new MailService(); //subscriber
var messageService = new MessageService(); //subscriber

videoEncoder.VideoEncoded += mailService.OnVideoEncoded; //Not making a call, just adding that event to the list of subscribers
videoEncoder.VideoEncoded += messageService.OnVideoEncodedMessage;


videoEncoder.Encode(video);
