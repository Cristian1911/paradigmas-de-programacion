﻿using static System.Net.Mime.MediaTypeNames;

public class VideoEventArgs : EventArgs
{
    public Video? Video { get; set; }
}

public class VideoEncoder
{
    /*Para generar eventos desde este método:
     * 1- Definir un Delegate
     * 2- Definir el evento basado en el delegate
     * 3- Raise the event
    */


    #region event

    //origen: Class that is publishing
    //e: addiotional data sent with the event
    //public delegate void VideoEncodedEventHandler(object origen, string text);
    //(object origen, VideoEventArgs args);

    //public event VideoEncodedEventHandler? VideoEncoded;
    public event EventHandler VideoEncoded;
    //public event EventHandler<String>? VideoEncoded;


    #endregion
    public void Encode(Video video)
    {
        Console.WriteLine("Encoding video...");
        Thread.Sleep(3000);

        //Then:
        //4- We call the event publisher method:
        OnVideoEncoded(video);
    }

    //event publisher method
    protected virtual void OnVideoEncoded(Video video)
    {
        if(VideoEncoded != null)
        {
            //Quién publica el evento? Quién lo produce?
            //La clase actual. La representamos con this
            VideoEncoded(this, EventArgs.Empty);


            //VideoEncoded(this, new VideoEventArgs() { Video = video });
        }
        else { Console.WriteLine("no hay suscriptores"); }
    }
}