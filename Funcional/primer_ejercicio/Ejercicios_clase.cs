//!--------------------------------------------Recursividad--------------------------------------------

/*
1- Crea una función "Potencia" que reciba dos parámetros: número y potencia.
El resultado debe ser el número elevado a esa potencia. Por ejemplo: Potencia(2,3) = 8   porque 2*2*2 = 8
Deben usar recursividad.
Solo se puede usar multiplicación simple.
*/

/*
2- Crea una función factorial.
Dado un número, calcular su factorial. (😅)
El factorial de un número se representa con un !
Por lo tanto, el factorial de n sería n!
n! = n * (n-1) * (n-2) * ... * 1
3! = 3 * 2 * 1 => 6 
*/

/*
3- Crea una función que reverse un texto: Reverse("hola") = aloh
No se debe alterar el texto inicial, sino retornar uno nuevo.
Se debe usar recursividad. 
Apóyense del método Substring.
Tip: Agarrar el primer caracter del texto y sumarlo al final de los caracteres restantes:
Hola -> ola + h. (primer paso)
*/

/*
4- Crea una función que valide si una palabra es o no un palíndromo.
Un palíndromo es una palabra que se escribe igual al derecho y al revés.
ana
arenera
acurruca
reconocer

La función debe retornar true o false dependiendo de si la palabra es o no un palíndromo.
*/

/*
5- Crea una función que encuentre el número más grande de una lista de números.
Necesito iterar sobre la lista y estar comparando la posición actual con la siguiente.
Además, debo tener una variable que esté almacenando el valor mayor de cada comparación.
Se necesitan: La lista de números, el valor máximo actual y el índice.

Se crearán dos funciones:
- Una se llama solo con la lista de números y es encargada de invocar a la función que realiza la iteración
- La otra se encarga de la comparación de valores y de iterar sobre ella misma.

Reto: Creen la primera función como una variable de tipo función.
*/


//!--------------------------------------------Funciones de primera clase--------------------------------------------

/*
6- Crea una función, factory, que toma un número como parámetro y retorna otra función.

La función retornada debe tomar una lista de números como parámetro, y retornar una lista con esos números multiplicados por el numero que se le dió a la primera función.

Aquí usaremos linq. Select.
*/