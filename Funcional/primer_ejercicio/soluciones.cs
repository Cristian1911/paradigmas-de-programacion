#region Ejercicio #1: Potencia
int NumberToPower(int number, int power)
{
    if (power == 0)
    {
        return 1;
    }
    else if (power > 1)
    {
        return number * NumberToPower(number, power - 1);
    }
    return number;
}
#endregion

#region Ejercicio #2: Factorial
int Factorial(int number)
{
    if (number > 0)
    {
        return number * Factorial(number - 1);
    }
    return 1;
}

#endregion

#region Ejercicio #3: Reverse String
string Reverse(string text)
{
    if (text.Length > 1)
    {
        char firstCharacter = text[0];
        string rest = text.Substring(1);

        return Reverse(rest) + firstCharacter;
    }
    return text;
}

#endregion

#region Ejercicio #4: Palíndromo
bool Palindrome(string text)
{
    if (text.Length <= 1)
    {
        return true;
    }
    if (text[0] == text[text.Length - 1])
    {
        return Palindrome(text.Substring(1, text.Length - 2));
    }
    return false;
}

#endregion

#region Ejercicio #5: Máximo de lista

#region Forma uno: Función como variable y luego iterador.

Func<List<int>, int> MaxNumberV = (numbers) => MaxNumberIteration(numbers, numbers[0]);

int MaxNumberIteration(List<int> numbers, int currentMax, int index = 1)
{
    if (index != numbers.Count)
    {
        int newMax = (currentMax > numbers[index]) ?
        currentMax :
        numbers[index];

        return MaxNumberIteration(numbers, newMax, index + 1);
    }
    return currentMax;
}

#endregion

#region Forma dos: Una sola función

int MaxNumber(List<int> nums, int index = 0)
{
    if(index < nums.Count-1) 
    {
        int result = MaxNumber(nums, index + 1);
        return (nums[index] > result) ? nums[index] : result;
    }
    return nums[index];
}

List<int> numbers = new List<int> { 1, 5, 7, 10, 1, 4, 80 };

Console.WriteLine(MaxNumber(numbers));

#endregion

#endregion

#region Ejercicio #6: Función factory

Func<int, Func<List<int>, List<int>>> factoryV = (multiplier) => nums => nums.Select(num => num * multiplier).ToList();

Func<List<int>, List<int>> FactoryF(int multiplier)
{
    return (numbers) => numbers.Select(num => num * multiplier).ToList();
}

Func<List<int>, List<int>> times = factoryV(7);


Console.WriteLine(times(numbers)[2]);
Console.WriteLine(factoryV(7)(numbers)[2]);
Console.WriteLine(FactoryF(7)(numbers)[1]);

#endregion