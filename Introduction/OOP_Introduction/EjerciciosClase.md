# Ejercicio 1: Crear una clase Persona
## Enunciado:
Crea una clase llamada Persona que tenga los siguientes atributos:
- Nombre (string)
- Edad (int)

La clase debe tener:
- Un constructor que reciba el nombre y la edad.
- Un método Saludar() que imprima en consola: "Hola, mi nombre es [Nombre] y tengo [Edad] años."

Luego, crea una instancia de la clase Persona y llama al método Saludar().


# Ejercicio 2: Crear una clase Coche
## Enunciado:
Crea una clase llamada Coche que tenga los siguientes atributos:
- Marca (string)
- Modelo (string)
- Año (int)

La clase debe tener:
- Un constructor que reciba la marca, el modelo y el año.
- Un método MostrarDetalles() que retorne un string así: "Este coche es un [Marca] [Modelo] del año [Año]."

Luego, crea dos instancias de la clase Coche y llama al método MostrarDetalles() para cada una.


# Ejercicio 3: Crear una clase Rectangulo
## Enunciado:
Crea una clase llamada Rectangulo que tenga los siguientes atributos:
- Ancho (double)
- Alto (double)

La clase debe tener:
- Un constructor que reciba el ancho y el alto.
- Un método CalcularArea() que devuelva el área del rectángulo (ancho * alto).
- Un método CalcularPerimetro() que devuelva el perímetro del rectángulo (2 * (ancho + alto)).

Luego, crea una instancia de la clase Rectangulo y muestra en consola el área y el perímetro.