﻿#region CuentaBancaria
//CuentaBancaria cuenta = new CuentaBancaria();

//Console.WriteLine(cuenta.GetSaldo());
//cuenta.Depositar(5000);
//Console.WriteLine(cuenta.GetSaldo());
//cuenta.Retirar(7000);
//cuenta.Retirar(26);

//Console.WriteLine(cuenta.GetSaldo()); 

#endregion

public class CuentaBancaria
{
    private double saldo;

    public CuentaBancaria(double saldo = 0)
    {
        this.saldo = saldo;
    }

    public void Depositar(double cantidad)
    {
        saldo += cantidad;
        Console.WriteLine($"Depósito exitoso. Nuevo saldo: {saldo}");
    }

    public void Retirar(double cantidad)
    {
        if (cantidad > saldo)
        {
            Console.WriteLine("Saldo disponible insuficiente.");
            return;
        }
        saldo -= cantidad;
        Console.WriteLine($"Retiro exitoso. Saldo restante: {saldo}");
    }

    public double GetSaldo()
    {
        return saldo;
    }
}

public class Producto
{
    public string nombre;
    public double precio;

    public Producto(string nombre, double precio = 0)
    {
        this.nombre = nombre;
        this.precio = precio;
    }

    public void ActualizarPrecio(double nuevoPrecio)
    {
        if (precio < 0)
        {
            Console.WriteLine("El precio del producto debe ser mayor a cero.");
            return;
        }
        precio = nuevoPrecio;
    }

    public void AplicarDescuento(double descuento)
    {
        if (descuento > 0 && descuento < 100)
        {
            precio *= 1 - (descuento / 100);
            return;
        }
        Console.WriteLine("El porcentaje de descuento debe ser un valor entre 1 y 99");
    }
}