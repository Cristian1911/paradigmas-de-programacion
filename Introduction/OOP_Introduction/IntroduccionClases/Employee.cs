﻿public class Employee
{
    public double baseSalary;
    public double vacationDays;
    public double overtimeHours;
    private double seniorityDays;

    public Employee(double baseSalary, double overtime, double vacations, double seniority)
    {
        this.baseSalary = baseSalary;
        this.vacationDays = vacations;
        this.overtimeHours = overtime;
        this.seniorityDays = seniority;
    }

    public double CalculateCompensation(int dayOfMonth)
    {
        double labouredDaysCompensation = DailySalary() * dayOfMonth;
        double overtimeCompensation = (3 * HourlySalary()) * overtimeHours;
        double vacationsCompensation = (2 * DailySalary()) * vacationDays;

        double finalCompensation = labouredDaysCompensation + overtimeCompensation + vacationsCompensation;

        return finalCompensation * YearlyCompensation();
    }

    private double DailySalary()
    {
        return baseSalary / 30;
    }

    private double HourlySalary()
    {
        return DailySalary() / 8;
    }

    private double YearlyCompensation()
    {
        double compensation_factor = 0.15;
        double years = Math.Floor(seniorityDays / 365);

        return 1.0 + (years * compensation_factor);
    }
}

