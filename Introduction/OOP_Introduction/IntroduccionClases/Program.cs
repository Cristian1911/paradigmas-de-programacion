Employee e = new Employee(5000, 5, 10, 823);

Console.WriteLine(e.CalculateCompensation(12));

var employee = new Dictionary<string, double>
{
    {"salary", 5000},
    {"vacations", 10},
    {"seniority", 823},
    {"overtime", 5}
};

Console.WriteLine($"Indemnización: {CalculateCompensation(employee,12)}");


double GetDailySalary(Dictionary<string, double> empleadoParametro)
{
    double salary = empleadoParametro["salary"] / 30;
    return salary;
}

double GetHourlySalary(Dictionary<string, double> empleadoParametro)
{
    return GetDailySalary(empleadoParametro) / 8;
}

double YearlyCompensation(Dictionary<string, double> empleadoParametro)
{
    double compensation_factor = 0.15;
    double years = empleadoParametro["seniority"] / 365;
    years = Math.Floor(years);

    return 1.0 + (years * compensation_factor);
}

double CalculateCompensation(Dictionary<string, double> empleadoParametro, int day_of_month)
{
    // Cantidad de días trabajados
    double final_compensation = GetDailySalary(empleadoParametro) * day_of_month;

    // Cantidad de horas extra
    final_compensation += GetHourlySalary(empleadoParametro) * empleadoParametro["overtime"] * 3;

    // Vacaciones acumuladas
    final_compensation += GetDailySalary(empleadoParametro) * empleadoParametro["vacations"] * 2;

    // Se multiplica por el seniority
    final_compensation *= YearlyCompensation(empleadoParametro); //1.30

    return final_compensation;
}