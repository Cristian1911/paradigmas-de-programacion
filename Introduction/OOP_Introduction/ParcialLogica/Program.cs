﻿Console.WriteLine(PintarTriangulo(9));
string[] juego = Jugar21(4);
foreach(var res in juego)
{
    Console.WriteLine(res);
}

string PintarTriangulo(int tamano){
    string triangulo = "";
    string espacio = "";
    for(int i = tamano; i > 0; i-=2){
        for (int j = i; j > 0; j--)
        {
            triangulo += "*";
        }
        espacio += " ";
        triangulo += $"\n{espacio}";
    }
    return triangulo;
}

string[] Jugar21(int jugadores)
{
    string[] resultados = new string[jugadores];
    // int[] puntajes = new int[jugadores];
    int puntajeActual;
    Random carta = new Random();

    for(int i=0; i<jugadores; i++)
    {
        puntajeActual = 0;
        for (int j = 0; j < 3; j++)
        {
            puntajeActual += carta.Next(1,12);
        }
        // puntajes[i] = puntajeActual;
        resultados[i] = $"Jugador {i+1}, puntos {puntajeActual} -> {DarResultado(puntajeActual)}";
    }

    return resultados;
}

string DarResultado(int puntaje){
    if(puntaje == 21)
    {
        return "Juego perfecto!";
    }
    else if(puntaje < 21)
    {
        return "Faltaron puntos.";
    }
    return "Se pasó. :(";
}