﻿public class Estudiante
{
    public string Nombre;
    private int Edad;
    private static int total_estudiantes;
    public static int TotalEstudiantes{
        get { return total_estudiantes; }
    }

    public Estudiante(string nombre, int Edad)
    {
        this.nombre = nombre;
        this.Edad = Edad;
        this.total_estudiantes++;
    }
}

/*
public class Estudiante
{
    private string _nombre;
    private int _edad;
    private static int _totalEstudiantes;

    public string Nombre{
        get { return _nombre; }
    }

    public int Edad{
        get { return _edad; }
    }

    public static int TotalEstudiantes{
        get { return _totalEstudiantes; }
    }

    public Estudiante(string nombre, int edad)
    {
        this._nombre = nombre;
        this._edad = edad;
        this._totalEstudiantes++;
    }
}
*/