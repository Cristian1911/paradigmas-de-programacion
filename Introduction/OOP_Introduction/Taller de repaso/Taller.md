# 1. Clase
Una **_clase_** es la **_plantilla o molde_** a partir del cual **_se crean objetos_**. Define la estructura (atributos o campos) y el comportamiento (métodos) de los objetos. En C#, la sintaxis básica incluye el uso de palabras clave como class, la definición de atributos y métodos.

## Preguntas
- ¿Qué es una clase y cuál es su función en la programación orientada a objetos?
- ¿Cuáles son los componentes básicos de una clase en C#?
- ¿Cómo se define la sintaxis básica de una clase? ¿Qué rol cumplen los atributos y métodos?

## Ejercicios:

### 1.1 Clase Persona:
Crea una clase llamada Persona con los siguientes miembros:
Dos atributos públicos: 
- `nombre: string`
- `edad: int`

Un método `Saludar()` que imprima en consola:
"Hola, mi nombre es [nombre] y tengo [edad] años.".

### 1.2 Clase Calculadora:
Crea una clase Calculadora con:
Dos atributos públicos: 
- `numero1: double`
- `numero2: double`

Dos métodos públicos:
- `Sumar(): double` que devuelva la suma de los dos números.
- `Multiplicar(): double` que devuelva el producto de los dos números.

---

# 2. Instancia
Una **_instancia_** es un **_objeto_** creado a partir de una **_clase_**. Mientras que la clase es el plano, la instancia es la realización concreta, es decir, el objeto que se utiliza en el programa. Cada instancia tiene su propio estado y puede interactuar mediante los métodos definidos en la clase.

## Preguntas
- ¿Qué es una instancia y cómo se relaciona con la clase?
- ¿Cuál es la diferencia entre la definición de una clase y la creación de una instancia?
- ¿Cómo se crea una instancia en C# y qué significa el operador new?

## Ejercicios
_Continuamos con los ejercicios del tema anterior_
### 2.1 Clase Persona
- Crea una instancia de Persona llamada `persona1`.
- Asigna los valores `"Ana"` y `25` a sus atributos.
- Llama al método `Saludar()` para ver el mensaje.

### 2.2 Clase Calculadora
- Crea dos instancias de Calculadora: `calcA` y `calcB`.
- Para `calcA`, asigna `10` y `5` a sus atributos.
- Para `calcB`, asigna `3` y `7`.
- Imprime la suma de `calcA` y la multiplicación de `calcB`.

---

# 3. Encapsulamiento
El encapsulamiento es el mecanismo que permite **_ocultar_** los detalles internos de una clase y **_exponer solo lo necesario_** mediante métodos y propiedades. Se utiliza para proteger el estado interno del objeto y **_garantizar que se acceda_** a él **_de forma controlada_**. En C#, se implementa mediante modificadores de acceso como `private`, `public`, entre otros.

## Preguntas
- ¿Qué se entiende por encapsulamiento y por qué es importante en la POO?
- ¿Cómo se utilizan los modificadores de acceso para implementar el encapsulamiento en C#?
- ¿Cuáles son las ventajas de ocultar los atributos y exponerlos mediante métodos o propiedades?

## Ejercicios
### 3.1 Clase Persona
Modifica la clase Persona para que:
- Los atributos `nombre` y `edad` sean `privados`.
- Agrega métodos públicos:
    - `SetNombre(string nuevoNombre)` para asignar el nombre.
    - `GetNombre(): string` para obtener el nombre.
    - `SetEdad(int nuevaEdad)` con validación (edad no puede ser negativa).

### 3.2 Clase Cuenta Bancaria
Crea una clase CuentaBancaria con:
Un atributo privado:
- `saldo: double`
Métodos públicos:
- `Depositar(double monto)` para aumentar el saldo.
- `Retirar(double monto)` que no permita retiros mayores al saldo.
- `ConsultarSaldo()` que devuelva el saldo actual.
---

# 4. Propiedades
Las propiedades en C# ofrecen una forma controlada de acceder y modificar los campos privados de una clase. Permiten incluir lógica adicional en los métodos getter y setter, lo que facilita la validación y el mantenimiento de la integridad de los datos.

## Preguntas
- ¿Qué son las propiedades y cómo se diferencian de los campos (atributos) tradicionales?
- ¿Cuáles son las ventajas de usar propiedades en lugar de exponer campos públicos?
- ¿Cómo se implementa una propiedad en C# y qué roles cumplen los métodos get y set?

## Ejercicios
### 4.1 Clase Persona
Reemplaza los métodos `GetNombre()` y `SetNombre()` de la clase `Persona` por una propiedad `Nombre` con `getter` y `setter`. Haz lo mismo para `Edad`, validando que no sea negativa en el `setter`.

### 4.2 Clase Libro
Crea una clase Libro con:
- Una propiedad `Titulo` de solo lectura (solo getter).
- Una propiedad `Paginas` con un `setter` que valide que el valor sea mayor a 0.
- Un constructor que reciba `título` y `páginas`.

### 4.3 Clase Estudiante
- Define una clase `Estudiante` con las propiedades `Nombre` y `Edad`.
- Para la propiedad `Edad`, implementa una validación en el `setter` que no permita asignar valores negativos.
- En el método Main, crea una **instancia** de `Estudiante`, asigna valores a las propiedades y muestra la información del estudiante.

# 5. Static
Los miembros estáticos **_pertenecen a la clase_** en sí y **_no a una instancia particular_**. Esto significa que se pueden acceder sin necesidad de crear un objeto de la clase. Son útiles para representar información o métodos que deben ser comunes a todas las instancias o que no dependen de un estado particular.

## Preguntas
- ¿Qué significa que un miembro de una clase sea static?
- ¿En qué situaciones es conveniente utilizar miembros estáticos?
- ¿Cómo se accede a un miembro estático y en qué se diferencia de un miembro de instancia?

## Ejercicios
### 5.1 Clase Contador
Crea una clase `Contador` con:
- Un campo estático `conteo (int)` inicializado en 0.
- Un método estático `Incrementar()` que aumente conteo en 1.
- Un método estático `ObtenerConteo()` que devuelva el valor actual.
En el Main, llama tres veces a Incrementar() y muestra el resultado.

### 5.2 Clase UtilesMatemáticos
Crea una clase `UtilesMatematicos` con:
- Un método estático `CalcularAreaCirculo(double radio): double` que devuelva el área.
- Un método estático `CalcularFactorial(int n): int` que calcule el factorial de n.
En el Main, usa ambos métodos para radio=5 y n=5.