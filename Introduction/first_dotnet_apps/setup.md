# Install .NET 8.0
https://dotnet.microsoft.com/es-es/download

# Install VS Code
https://dotnet.microsoft.com/es-es/learntocode
## Download .exe
https://code.visualstudio.com/Download

## Install C# Dev Kit extension
https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.csdevkit&WT.mc_id=dotnet-35129-website
- Verificar corriendo <dotnet --version> en la consola

# Crear primer proyecto:
## Via Vs Code:
- Abrir ventana de comandos/atajos de VS Code: <ctrl + shift + P> o <F1> 
- .NET: New Project -> Console App -> Asignarle un nombre ("FirstConsoleApp") -> Default Settings
- Esto crea una carpeta con el proyecto y un .sln con la información de los proyectos que existen en el workspace actual


# Via Comandos:
Con estos comandos crearemos nuevos proyectos y los usaremos dentro de la misma ventana
- Create a new console app
<dotnet new console -n MyConsoleApp>

- Add the project to the solution
<dotnet sln add MyConsoleApp>

- Run the application
<dotnet run --project MyConsoleApp>


# COMO CREAR VARIOS PROYECTOS EN LA MISMA VENTANA DE VISUAL STUDIO??