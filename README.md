# OOP Cheatsheet - Python

## 1. Estructura de una Clase

```python
class MyClass:
    class_attribute = "I am a class attribute"

    #Constructor / Inicializador
    def __init__(self, instance_attribute):
        self.instance_attribute = instance_attribute  # Atributo de instancia

    def instance_method(self, x: int) -> int:
        print(f"This is an instance method with instance attribute: {self.instance_attribute}")
        return 1

    @classmethod
    def class_method(cls):
        print(f"This is a class method: {cls.class_attribute}")

    @staticmethod
    def static_method():
        print("This is a static method")
```

## 2. Encapsulamiento

### En Python, no existen niveles estrictos de acceso como en C#. Se utilizan convenciones:

### - **Public:** Acceso sin restricciones.
### - **Protected:** Un underscore (_) indica que el atributo o método es protected (convención, puede ser accedido).
### - **Private:** Dos underscores (__) para un atributo o método privado.

```python
class EncapsulationExample:
    def __init__(self):
        self.public = "I am public"
        self._protected = "I am protected"
        self.__private = "I am private"

    def get_private(self):
        return self.__private

```
### - `self.public`: Atributo público.
### - `self._protected`: Atributo protegido (acceso interno o de subclases).
### - `self.__private`: Atributo privado (acceso restringido).


## 3. Herencia
### - Python soporta herencia simple y múltiple.

```python
class Parent:
    def parent_method(self):
        print("This is the parent method")

class Child(Parent):  # Hereda de Parent
    def child_method(self):
        print("This is the child method")

```


## 4. Getters y Setters
### - Python tiene un enfoque más sencillo usando *decoradores* de `properties` en lugar de métodos explícitos de `get` y `set` como en C#.

```python
class Person:
    def __init__(self, name):
        self._name = name

## GETTER
    @property
    def name(self):
        return self._name
## SETTER
    @name.setter
    def name(self, value):
        if isinstance(value, str):
            self._name = value
        else:
            raise ValueError("Name must be a string")
```

## 5. Métodos Estáticos y de Clase

### Método estático
```python
class MathOperations:
    @staticmethod
    def add(x, y):
        return x + y
```

###  Método de clase
```python
class MyClass:
    @classmethod
    def create_instance(cls):
        return cls()
```

### *¿CUÁL ES LA DIFERENCIA ENTRE MÉTODOS DE CLASE Y MÉTODOS ESTÁTICOS?*

## 6. Sobrecarga de Métodos (Overload)
### Python no admite sobrecarga como C#, pero puedes usar argumentos por defecto como `*args` o `**kwargs` para simular la sobrecarga.

```python
class OverloadExample:
    def method(self, *args):
        if len(args) == 1:
            print(f"One argument: {args[0]}")
        elif len(args) == 2:
            print(f"Two arguments: {args[0]}, {args[1]}")
```

## 7. Sobreescritura de Métodos (Override)
### Un método de una clase hija **siempre puede sobreescribir** un método de la clase padre.

```python
class Parent:
    def method(self):
        print("Parent method")

class Child(Parent):
    def method(self):
        print("Child method")
```

## 8. Métodos Virtuales y Abstractos
### **Métodos Virtuales**
### - En Python, **todos los métodos son virtuales** por defecto, lo que significa que pueden ser sobrescritos en clases derivadas.

### **Métodos Abstractos (con `ABC`)**
### - Para definir clases y métodos abstractos, usamos el módulo `ABC`.
```python
from abc import ABC, abstractmethod

class AbstractClass(ABC):
    @abstractmethod
    def abstract_method(self):
        pass

class ConcreteClass(AbstractClass):
    def abstract_method(self):
        print("Implementing abstract method")
```


## Limitaciones con respecto a C#

### **Encapsulamiento:** 
- No es tan estricto como en C#. En Python, se utiliza una convención en lugar de control real de acceso a atributos.

### **Sobrecarga de métodos:** 
- Python no tiene soporte nativo para la sobrecarga de métodos como C#. Se usa *args y **kwargs para simularlo.

### **Interfaces:** 
- No existen interfaces como en C#, pero puedes simular su comportamiento usando clases abstractas.

### **Tipos estáticos:** 
- Python es dinámicamente tipado, mientras que C# es fuertemente tipado, lo que significa que no tienes verificación de tipos estricta en tiempo de compilación.

### **Propiedades:** 
- En C#, los getters y setters se definen explícitamente, mientras que en Python se usan decoradores como @property y @setter.